# Migrated

This project migrated to Codeberg, the new URL is https://codeberg.org/nerdeiro/ansible-kubernetes

## ansible_k8s

This playbook helps create a kubernetes cluster.

### Configuring

To use it, configure the inventory file (`inventory.cfg`) with your hosts, but do not change the name of the 
groups in there, since the group names are used across the playbook to control where
some tasks should or should no run.

Place one, and only one, host on the group `controller` and the others on the `nodes` group.

**Atention:** It is **HIGLY** recomended that you run this Playbook on *clean* installs
of Debian Bookworm. Hosts that are already in use for other stuff or distros
based on, but that are not a pure, Debian might not work as expected.

### Running

You run the playbokk with the command:

    ansible-playbook -i inventory.cfg --tags <some tag> clear main.yaml

See bellow for the tags you can use.

### Playbook Tags

Since the Playbook is divided in many roles, you should use one of the following tags
to select which ones will run.

#### Tag: install

the tag `install` does exactly what it says. It will install some needed packages from
Debian's repository, configure the OS with some stuff required by Kubeadm, add K8s repos
and install Kubeadm, Kubectl and Kubelet on the control node, as well as configuring them for you.

This tag will install Kubelet on runner nodes, but will not join the to the cluster. You can
use this tag togheter with `join` to do it in one go, but I recommend doing it separately.

#### Tag: join

This tag will join nodes to the cluster. It will first generate a token on the control node,
then use it to run a `kubeadm join` on the other nodes. It's simple as that.

#### Tag: clear

If you run the playbook with the tag `join` many times, you'll end up with a bunch of
stale tokens. Run the playbook with this tag to clean them.

### Disclaimer

This is a simple Playbook aiming at setting up a small cluster for learning purposes.

If you use it for anything remotely resembling production, you are definetely on you own,
don't blame me for anything bad that happens because of it.

Happy learning, and if you want to talk about this Playbook, Ansible or K8s in general,
you can find me on [Mastodon](https://fosstodon.org/@nerdeiro).


---
- name: configure kernel
  tags: install
  block:
    - name: configure loading of br_netfilter
      # this kernel module is necessary, but kubeadm doesn't
      # load it by itself
      ansible.builtin.lineinfile:
        path: /etc/modules
        line: 'br_netfilter'

    - name: force loading of br_netfilter
      community.general.modprobe:
        name: br_netfilter
        state: present

    - name: configure sysctl
      # Allows ip forwarding
      ansible.builtin.blockinfile:
        backup: true
        create: true
        state: present
        insertafter: "EOF"
        marker: "### {mark} KUBERNETES SETTINGS BLOCK ###"
        path: /etc/sysctl.conf
        block: |
          net.bridge.bridge-nf-call-iptables = 1
          net.ipv4.ip_forward = 1

    - name: reload sysctl
      command: /sbin/sysctl -p

- name: swap settings
  # disables swap to avoid warnings
  tags: install
  block:
    - name: disable swap
      ansible.builtin.shell: swapoff -a

    - name: remove swap from fstab
      ansible.builtin.lineinfile:
        path: /etc/fstab
        regexp: 'swap'
        state: absent

- name: configure kube repos
  # adds the necessary apt repo to install k8s stuff
  tags: install
  block:
    - name: kubernetes repo key
      ansible.builtin.get_url:
        url: https://pkgs.k8s.io/core:/stable:/v1.29/deb/Release.key
        dest: /tmp/kubernetes-apt-keyring.key

    - name: install key
      ansible.builtin.shell: |
        cat /tmp/kubernetes-apt-keyring.key | gpg --dearmor -o /etc/apt/keyrings/kubernetes-apt-keyring.gpg
      args:
        creates: /etc/apt/keyrings/kubernetes-apt-keyring.gpg

    - name: add repo
      ansible.builtin.apt_repository:
        install_python_apt: true
        repo: "deb [signed-by=/etc/apt/keyrings/kubernetes-apt-keyring.gpg] https://pkgs.k8s.io/core:/stable:/v1.29/deb/ /"
        filename: 'kubernetes'
        state: present

    - name: install more packages
      # install containerd and k8s packages
      apt:
        install_recommends: false
        update_cache: true
        policy_rc_d: 101
        state: latest
        pkg:
          - containerd
          - kubelet
          - kubeadm
          - kubectl
          - nfs-common

- name: configure containerd stuff
  # configure containerd to use systemd cgroups
  tags: install
  block:
    - name: make sure containerd is NOT running
      ansible.builtin.systemd_service:
        name: containerd
        state: stopped

    - name: generate TOML file
      ansible.builtin.shell: |
        containerd config default > /etc/containerd/config.toml

    # example configuration to add an insecure registry to containerd
    - name: add insecure registry
      # add registry URL
      ansible.builtin.blockinfile:
        backup: true
        create: true
        state: present
        insertafter: '\[plugins.\"io.containerd.grpc.v1.cri\".registry.mirrors\]'
        marker: "### {mark} INSECURE REGISTRY ###"
        path: /etc/containerd/config.toml
        block: |
          # HTTP registry URL
                [plugins."io.containerd.grpc.v1.cri".registry.mirrors."192.168.1.11"]
                  endpoint = ["http://192.168.1.11:3000"]

    - name: disable TLS check on registry
      # set skip verify
      ansible.builtin.blockinfile:
        backup: true
        create: true
        state: present
        insertafter: '\[plugins.\"io.containerd.grpc.v1.cri\".registry.configs\]'
        marker: "### {mark} INSECURE REGISTRY TLS ###"
        path: /etc/containerd/config.toml
        block: >
          # Disables TLS verification
                [plugins."io.containerd.grpc.v1.cri".registry.configs."192.168.1.11".tls]
                  insecure_skip_verify = true

    - name: fixes cgroup
      ansible.builtin.lineinfile:
        path: /etc/containerd/config.toml
        backrefs: true
        regexp: '^(.*)(SystemdCgroup).*=.*'
        line: '\1\2 = true'

    - name: starts containerd
      ansible.builtin.systemd_service:
        name: containerd
        state: started
